# Spring Boot to Google Cloud Run Deployment Guide

## Overview

This README outlines the process of packaging a Spring Boot application, containerizing it with Docker, and deploying it to Google Cloud Run.

## Prerequisites

- Maven
- Docker
- Google Cloud SDK

## Packaging with Maven

```sh
mvn clean package
```

## Building Docker Image

Dockerfile example:
```Dockerfile
FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```

Build command:
```sh
docker build -t your-app-name:v1 .
```

## Running Locally

```sh
docker run -p 8080:8080 your-app-name:v1
```

## Pushing to Google Container Registry

```sh
docker tag your-app-name:v1 gcr.io/your-project-id/your-app-name:v1
docker push gcr.io/your-project-id/your-app-name:v1
```

## Deploying to Google Cloud Run

```sh
gcloud run deploy your-service-name --image gcr.io/your-project-id/your-app-name:v1 --platform managed --region your-region --allow-unauthenticated
```

## Accessing Your Service

The deployment process will provide you with a URL to access your service.

## Notes

- Replace `your-app-name`, `v1`, `your-project-id`, `your-service-name`, and `your-region` with your specific details.
- Test your application locally before deploying.
- Ensure the Cloud Run API is enabled in your GCP project.

