# Docker Commands Guide

This guide provides a list of common Docker commands and their descriptions for quick reference.

## Getting Started

- `docker run [OPTIONS] IMAGE [COMMAND] [ARG...]`: Run a new container from an image.
- `docker build [OPTIONS] PATH | URL | -`: Build an image from a Dockerfile.

## Managing Containers

- `docker ps [OPTIONS]`: List running containers.
- `docker stop [CONTAINER]`: Stop a running container.
- `docker start [CONTAINER]`: Start a stopped container.
- `docker restart [CONTAINER]`: Restart a container.
- `docker rm [CONTAINER]`: Remove one or more containers.

## Managing Images

- `docker images [OPTIONS] [REPOSITORY[:TAG]]`: List images.
- `docker pull [OPTIONS] NAME[:TAG|@DIGEST]`: Pull an image from a registry.
- `docker push [OPTIONS] NAME[:TAG]`: Push an image to a registry.
- `docker rmi [OPTIONS] IMAGE [IMAGE...]`: Remove one or more images.

## Networking

- `docker network create [OPTIONS] NETWORK`: Create a network.
- `docker network ls [OPTIONS]`: List networks.
- `docker network rm [NETWORK]`: Remove one or more networks.

## Volumes

- `docker volume create [OPTIONS] [VOLUME]`: Create a volume.
- `docker volume ls [OPTIONS]`: List volumes.
- `docker volume rm [VOLUME]`: Remove one or more volumes.

## Logs and Monitoring

- `docker logs [OPTIONS] CONTAINER`: Fetch the logs of a container.
- `docker stats [OPTIONS] [CONTAINER...]`: Display a live stream of container(s) resource usage statistics.

## Docker Compose (for multi-container Docker applications)

- `docker-compose up [OPTIONS]`: Create and start containers as per the `docker-compose.yml` file.
- `docker-compose down [OPTIONS]`: Stop and remove containers, networks, images, and volumes.

For a detailed description of options and more commands, refer to the official Docker documentation. Remember to replace `[OPTIONS]`, `[CONTAINER]`, `[IMAGE]`, `[NETWORK]`, `[VOLUME]`, and `[REPOSITORY[:TAG]]` with your specific parameters.

---